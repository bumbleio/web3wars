import './App.css';
import React,{useState , useEffect} from 'react';
import { ethers } from 'ethers'
import w3w from './artifacts/contracts/w3w.sol/w3w.json'
import axios from 'axios';

// import components
import Nav from './components/nav'
import Header from './components/header';
import Statistics from './components/statistics';
import Owned from './components/owned';
import Footer from './components/footer';

var converter = require('hex2dec');





function App() {

   //polygon wew contract address
  const w3wAddress = "0xCa29fe9026c635e399d04A4fb386d902474f79a7";

  const [totalsupply, setTotalsupply] = useState('');
  const [apeimageurls, setApeimageurls] = useState([]);  


    // function to invoke metamask
  async function requestAccount() {
    await window.ethereum.request({ method: 'eth_requestAccounts' });
    console.log("clicked")
  }

  async function totalTokenSupply() {
    console.log("got to totalsupply function")
    if (typeof window.ethereum !== 'undefined') {
      await requestAccount()
      const provider = new ethers.providers.Web3Provider(window.ethereum)
      const contract = new ethers.Contract(w3wAddress, w3w.abi, provider)
        try {
          const data = await contract.totalSupply();
          var numberOfPassesOwned = converter.hexToDec(data._hex);
          setTotalsupply(numberOfPassesOwned);
          console.log("#passes: " + numberOfPassesOwned);
          // setContractname(data);
          //console.log('stateis' + contractname);
          //getAllTokenMetaData()
        } catch (err) {
          console.log("Error: ", err);
          alert('Please make sure you are using Metamask and are connected to Polygon Mainnet - https://docs.polygon.technology/docs/develop/metamask/config-polygon-on-metamask/');
        }
      }    
    }

    async function getAllTokenMetaData() {
      //resets state for each connect
      setApeimageurls([]);
      const nfts = [];
      if (typeof window.ethereum !== 'undefined') {
        const ipfsGatewayBase = "https://gateway.pinata.cloud/ipfs/"
        console.log("connecting to wallet")
        await requestAccount();
        // get address of wallet
        // add to variable and console log it const address
        const activeAddresswindow = await window.ethereum.request({ method: 'eth_requestAccounts' });
        console.log(activeAddresswindow[0]);
   
        //with address variable call contract function balaceof to find out how many tokens of the contact it owns add number to variable
        // const numberOfTokensOwned
        const provider = new ethers.providers.Web3Provider(window.ethereum);
        console.log("providerset");
        const contract = new ethers.Contract(w3wAddress, w3w.abi, provider)
        console.log("providerset11");
        const hexOfTokensOwned = await contract.balanceOf(activeAddresswindow[0]);
        var numberOfTokensOwned = converter.hexToDec(hexOfTokensOwned._hex);
        console.log("tokensowned: " + numberOfTokensOwned)
        console.log(typeof numberOfTokensOwned);
        parseInt(numberOfTokensOwned);
        
        for (let i = 0; i < parseInt(numberOfTokensOwned); i++) {
          console.log("The number is " + i);
          const tokenIndexOwned = await contract.tokenOfOwnerByIndex(activeAddresswindow[0],i);      
          const tokenURI = await contract.tokenURI(converter.hexToDec(tokenIndexOwned._hex));
         // request(ipfsGatewayBase + tokenURI.substring(7) , { json: true }, (err, res, body) => {
           // if (err) { return console.log(err); }
             // console.log(ipfsGatewayBase + body.image.substring(7))
             // setApeimageurls(apeimageurls => apeimageurls.concat(ipfsGatewayBase + body.image.substring(7)));
            //});  


            // need to add error checking here if not 200 respond then
            const result = await axios(
              ipfsGatewayBase + tokenURI.substring(7),
            );
                console.log(result.data.image);
                console.log(result.data.attributes[0].value)
                console.log(result.data.attributes[1].value)
                console.log(ipfsGatewayBase + result.data.image.substring(7))
                //setApeimageurls(apeimageurls => apeimageurls.concat("dddd sa"));

                //console.log("array " + apeimageurls)



                const nftobj = {
                image: ipfsGatewayBase + result.data.image.substring(7),
                clan: result.data.attributes[0].value,
                  rank: result.data.attributes[1].value 
                };
                nfts.push(nftobj);
                console.log("nft :" + nfts[i])

                console.log(nftobj)
                // setApeimageurls(apeimageurls => apeimageurls.concat(nftobj));

                // console.log("arra " + apeimageurls)
                //console.log(nftobj)






        }

        setApeimageurls(apeimageurls => apeimageurls.concat(nfts));


      }
    }

  return (
    <div className="container">
      <Nav onClickconnect={getAllTokenMetaData}/>
      <Header />
      <Statistics  totalsupply={totalsupply} />
      <Owned apeimageurls={apeimageurls}/>
      <Footer />
    </div>
  );
}

export default App;
