import React, { useEffect } from 'react';
import axios from 'axios';

// https://www.robinwieruch.de/react-hooks-fetch-data/

const Statistics = ({totalsupply}) => { 



    return (

<div className="statistics">
    <div className="title">
        <h1>Collection Statistics</h1>
    </div>
    <div className="items">
        <h3>Contract:</h3> <h5>0xCa29fe9026c635e399d04A4fb386d902474f79a7</h5>
    </div>
    <div className="volume">
        <h3>Token Supply:</h3> <h5>5824</h5>
    </div>
    <div className="price">
        <h3>Chain:</h3> <h5>Polygon Mainnet</h5>
    </div>
    <div className="owners">
        <h3>Number of Owners:</h3> <h5>285</h5>
    </div>
</div>
    )
}


export default Statistics;