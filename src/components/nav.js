import React from 'react';
import logo from '../images/LOGOsmall.png'
import Button from './Button';



const Nav = ({onClickconnect}) => { 

    return (

<div className="nav">
  <div className="left">
  <img src={logo} alt="logo" className="logo"></img>

  </div>
  <div className="middle">
  </div>
  <div className="right">
  <Button color='Black' text='Connect' onClick={onClickconnect} classn='btn' />
  </div>
</div>
    )
}


export default Nav;