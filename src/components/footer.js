import React from 'react';
// get our fontawesome imports
// https://www.digitalocean.com/community/tutorials/how-to-use-font-awesome-5-with-react
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faDiscord} from "@fortawesome/free-brands-svg-icons"
import {faTwitter} from "@fortawesome/free-brands-svg-icons"
import {faMedium} from "@fortawesome/free-brands-svg-icons"




const Footer = () => { 

    return (

<div className="footer">
  <div className="left">
  

  </div>
  <div className="middle">
  
  <a href="https://mobile.twitter.com/web3wars" className='links'>
  <FontAwesomeIcon icon={faTwitter} size="4x"/>
  </a>    
  <a href="https://t.co/J4fGDVYPRU" >
  <FontAwesomeIcon icon={faDiscord} size="4x"/>
  </a>
  <a href="https://t.co/4u6JzKdq6c">
  <FontAwesomeIcon icon={faMedium} size="4x"/>
  </a>    

  </div>
  <div className="right">
  </div>
</div>
    )
}


export default Footer;