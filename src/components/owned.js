import React from 'react';
import Nft from './nft';




const Owned = ({apeimageurls}) => { 

    return (

<div className="owned">
        <div className="title">
            <h1>Memberships Owned</h1>
        </div>
        
        {apeimageurls.map((ape, index) => (
        <Nft key={index} apeimageurl={ape}/>
      ))}
       


    

</div>
    )
}


export default Owned;