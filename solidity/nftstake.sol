// SPDX-License-Identifier: MIT
pragma solidity ^0.8.2;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";
import "@openzeppelin/contracts/security/Pausable.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";

interface IERC721Token {
    function balanceOf(address owner) external view returns (uint256 balance);
    function name() external returns (string memory);
    function owner() external returns (string memory);
    function ownerOf(uint256 tokenId) external view returns (address);
}



contract MyToken is ERC20, ERC20Burnable, Pausable, Ownable {
    constructor(address _tokencontract) ERC20("MyToken", "FIGHT") {
        token = IERC721Token(_tokencontract);
        _mint(msg.sender, 1000 * 10 ** decimals());
    }

using SafeMath for uint256;
// set the NFT token contract address - this has to be changeable

uint256 public reward = 10 ether;
mapping (uint256 => address) public nftStakedBy;
mapping (address => uint256) public nftOwnedCount;
uint256[]public nfts;
address[] public stakeholders;
IERC721Token public token;


    function setNftContract(IERC721Token _token)
    public
    onlyOwner
    {
        token = _token;
    }


// stake migrate function after a transfer to new address

    function stakeMigrateWallet()
        public
        {
            
        }

    function balance() public view returns (uint256){
       uint256 _balance = token.balanceOf(0x5B38Da6a701c568545dCfcB03FcB875f56beddC4);
       return _balance;
       
    }



    function countStakeholders()
        public
        view
        returns(uint256)
    {
        return stakeholders.length;
    }

   function isStakeholder(address _address)
       public
       view
       returns(bool, uint256)
   {
       for (uint256 s = 0; s < stakeholders.length; s += 1){
           if (_address == stakeholders[s]) return (true, s);
       }
       return (false, 0);
   }


   function addStakeholder(address _stakeholder)
       public
   {
       (bool _isStakeholder, ) = isStakeholder(_stakeholder);
       if(!_isStakeholder) stakeholders.push(_stakeholder);
   }

    function removeStakeholder(address _stakeholder)
       public
   {
       (bool _isStakeholder, uint256 s) = isStakeholder(_stakeholder);
       if(_isStakeholder){
           stakeholders[s] = stakeholders[stakeholders.length - 1];
           stakeholders.pop();
       }
   }


// return number of NFT's staked

    function countNftStaked()
        public
        view
        returns(uint256)
    {
        return nfts.length;
    }


// checks if a token is staked
function nftisStaked(uint256 _tokenId)
public
view
returns(bool, uint256)
{

    for (uint256 s = 0; s < nfts.length; s += 1){
        if (_tokenId == nfts[s]) return (true, s);
    }
    return (false, 0);
}


function stakeNft(uint256 _tokenId)
    public
{
    // do a require that the address owns the NFT
    // add a interface to the ownerOf function on nft contract 0xCa29fe9026c635e399d04A4fb386d902474f79a7
    // require (nftContract.ownerOf(_tokenid) == msg.sender);
    // make sure it isnt already staked
    require (token.ownerOf(_tokenId) == msg.sender, "Caller is not the token owner");
    (bool _nftisStaked, ) = nftisStaked(_tokenId);
    if(!_nftisStaked){ 
        //validate order
        nfts.push(_tokenId); // array to track tokens which are staked
        nftStakedBy[_tokenId] = msg.sender;
        nftOwnedCount[msg.sender] = nftOwnedCount[msg.sender].add(1);
        addStakeholder(msg.sender);
    }
}

function unstakeNft (uint256 _tokenId)
    public
{
    // required msg.sender is the owner of the token
    require (token.ownerOf(_tokenId) == msg.sender, "Caller is not the token owner");
    (bool _nftisStaked, uint256 s) = nftisStaked(_tokenId);
       if(_nftisStaked){
           // validate order
           nfts[s] = nfts[nfts.length - 1];
           nfts.pop();
           // remove nftstaked mapping
           delete nftStakedBy[_tokenId];
           
           if (nftOwnedCount[msg.sender] == 1) {
               removeStakeholder(msg.sender);
           } 
           nftOwnedCount[msg.sender] = nftOwnedCount[msg.sender].sub(1);
       } 
}

mapping(address => uint256) public rewards;
// rewards

   function distributeRewards()
       public
       onlyOwner
   {
       for (uint256 s = 0; s < stakeholders.length; s += 1){
            address stakeholder = stakeholders[s];
            uint256 _nftsStaked = nftOwnedCount[stakeholder];
            uint256 _reward = _nftsStaked.mul(reward);
            rewards[stakeholder] = rewards[stakeholder].add(_reward);
       }
   }

 function setReward(uint256 _reward) public onlyOwner {
    reward = _reward;
  }

   function withdrawReward()
       public
   {
       uint256 _reward = rewards[msg.sender];
       rewards[msg.sender] = 0;
       _mint(msg.sender, _reward);
   }



// openzepplin contract below

    function pause() public onlyOwner {
        _pause();
    }

    function unpause() public onlyOwner {
        _unpause();
    }

    function mint(address to, uint256 amount) public onlyOwner {
        _mint(to, amount);
    }

    function _beforeTokenTransfer(address from, address to, uint256 amount)
        internal
        whenNotPaused
        override
    {
        super._beforeTokenTransfer(from, to, amount);
    }






}
// How many tokens is a person staking